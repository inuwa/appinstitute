const _ = require('lodash');
const Promise = require('bluebird');
/**
 * Returns the flattened Array
 * @param {*} req 
 */
var flattenArray = (req, res) => {
  let nestedArray = JSON.parse(req.body.nestedarray);
  console.log(nestedArray);
  if (_.isArray(nestedArray)) {
    return _.flattenDeep(nestedArray);
  }
  else {
    return 'Please enter a valid Array';
  }
};
/**
 * Returns view of Flattened Array
 * @param {*} req 
 * @param {*} res 
 */
var flattenedResults = ( req, res) => {
  return Promise.all([flattenArray(req, res)]).then((flattenedArray) => {
    res.render('home/nestedArray', {
      title: 'Flatten Array',
      flattenedArray: flattenedArray
    });
  }).catch((err) => {
    console.log('Error: ', err);
  });
};

/**
 * Displays the initial form view to enter the array
 * @param {*} req 
 * @param {*} res 
 */
var formView = (req, res ) => {
  res.render('home/nestedArray', {
    title: 'Flatten Array',
    csrf: req.csrf
  });
};
exports.flattenedResults = flattenedResults;
exports.formView = formView;