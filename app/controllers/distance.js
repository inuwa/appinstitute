const Promise = require('bluebird');
const jsonfile = require('jsonfile');
const geolib = require('geolib');
const path = require('path');

// Town Coordinates in Nothingham: lat: 52.951458, lon: -1.142332
// /customers/:latitude/:longitude/:distance
/**
 * Load the JSON file.
 * @param {File} file 
 */
var loadJson = (file) => {
  return new Promise((_resolve, _reject) => {
    jsonfile.readFile(file, (err, obj) => {
      if (err) {
        _reject(err);
      }
      _resolve(obj);
    });
  });
};
/**
 * Return customers close to the distance specified
 * @param {Array} customerDetails 
 * @param {Number} distanceInMiles 
 * @param {Object} townCoordinates 
 */
var getCustomersInArea = (customerDetails, distanceInMiles, townCoordinates) => {
  
  if (townCoordinates && townCoordinates.latitude && townCoordinates.longitude) {
    if (distanceInMiles) {
      let notNumbers = ((typeof townCoordinates.latitude === 'number') &&  (typeof townCoordinates.longitude === 'number') && (typeof distanceInMiles === 'number'));
      if (!notNumbers) {
        let selectedCustomers = customerDetails.map((customerDetail) => {
          if (customerDetail) {
            let distanceInMeters = geolib.getDistance(
              { latitude: townCoordinates.latitude, longitude: townCoordinates.longitude  },
              { latitude: customerDetail.location.lat, longitude: customerDetail.location.lon }
            );
            if (( distanceInMeters * 0.000621371) <= distanceInMiles ) {
              return customerDetail;
            }
          }
        });
        // removes null values
        selectedCustomers = selectedCustomers.filter(n => n);
        // returns the selected customers that are within the specified distance
        return selectedCustomers;
      } else {
          console.log('All values must be numeric; distance: %s; townCoordinates.latitude: %s townCoordinates.longitude: %s ', distanceInMiles, townCoordinates.latitude, townCoordinates.longitude);
      }
    } else {
      console.log('Distance must be a number');
    }
  } else {
    console.log('town coordinates must be declared');
  } 
};
/**
 * GET /customers/:latitude/:longitude/:distance
 * e.g /customers/52.951458/-1.142332/40
 * Returns REST API of customers within specified distance
 * @param {*} req 
 * @param {*} res 
 */
var selectedCustomers = (req, res) => {
  let distanceInMiles = req.params.distance;
  let townCoordinates = { latitude: req.params.latitude, longitude: req.params.longitude };
  let file = path.join(__dirname, '../staticfiles/customer_data.json'); 

  return Promise.all([loadJson(file)]).spread((customerDets) => {
    let customerDetails = customerDets;
    let customersInArea = getCustomersInArea(customerDetails, distanceInMiles, townCoordinates);
    return customersInArea;
  }).then((customersInArea) => {
    res.json(customersInArea);
  }).catch((err) => {
    console.log(err);
  });
};
exports.selectedCustomers = selectedCustomers;
// Make the data into an array push to the view res.json