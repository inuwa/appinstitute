## Nettitude Chart Applications
**Question 1**: What’s your proudest achievement? It can be a personal project or something you’ve worked on professionally. Just a short paragraph is fine, but I’d love to know why you’re proud of it.

**Answer**: My Proudest Achievement was when created my first iPhone App for my master's project. I came from a mechanical engineering background and I had very little prior programming experience. It was a challenge to myself that I could pick up new knowledge and deliver to a schedule, that is why it is my proudest moment.

**Question 2**: Tell me about a technical book, article or video you viewed recently, why you liked it, and why I should check it out.

**Answer**: A technical book that I think you should read is Clean Code by Robert C. Martin. It teaches you how to think when writing code and was released before Agile software development became a thing.

**Question 3**: Tell me about one feature of the AppInstitute App Builder/CMS you really like, and why.

**Answer**: I would say that the feature that I really like about the App Builder is the GUI interface. The fact that no code is needed to create a simple app is a boon. It reduces the headache and learning curve for the beginner.

**Question 4**: Write some code that will flatten an array of arbitrarily nested arrays of integers into a flat array of integers. e.g. [[1,2,[3]],4] -> [1,2,3,4].

**Answer**: Go to route: /flattenarray. Enter array value to see output of your array. *(See app installation instructions below)*

**Question 5**: We have some customer records in a text file (customer_data.json, attached) one customer per line, JSON-encoded. We want to invite any customer within 40 miles of our Nottingham office (GPS coordinates 52.951458, -1.142332) to some food and drinks on us. Write a program that will read the full list of customers and output the names of matching customers (within 40 miles), sorted by distance (ascending). Prioritise the structure of the code to demonstrate a clearly readable, maintainable and testable solution.

**Answer**: Go route: `/customers/:latitude/:longitude/:distance` e.g `/customers/52.951458/-1.142332/40`. You will get a response of the customers that are within the mileage. *(See app installation instructions below)*

## Usage


    $ git clone https://inuwa@bitbucket.org/inuwa/appinstitute.git
    $ cd appinstitute
    $ mongod
    $ npm install
    $ npm start
